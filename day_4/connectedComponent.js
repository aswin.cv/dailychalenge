//dfs() function performs dfs traversal  whic takes graph ,sourcenode and visited node as input
function dfs(graph, u, visited) {
    for (let v = 0; v < graph[u].length; v++) {
        if (visited[v] != 1 && graph[u][v] === 1) {
            visited[v] = 1;
            dfs(graph, v, visited);
        }
    }
}

//connectedComponent() takes graph as input and returns total no of connected component
function connectedComponent(graph) {
    let visited = [];
    let count = 0;
    for (let v = 0; v < graph.length; v++)
        visited[v] = 0;
    for (let v = 0; v < graph.length; v++) {
        if (visited[v] == 0) {
            dfs(graph, v, visited);
            count++;
        }
    }
    return count;
}

//main() function prints the no of connected component 
function main() {
    let graph = [[0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]

    result = connectedComponent(graph);
    console.log(result);
}
main();