/** Node class represent the datatype for  the tree node which contains value feild for data &
 * left feild for left node &right field for right node
 */
class Node {
    constructor(data) {
        this.left = null;
        this.value = data;
        this.right = null;
    }
}

/**getBinaryTree() returns the binary search tree */
function getBinaryTree() {
    root = new Node(1);
    root.left = new Node(2);
    root.right = new Node(3);
    root.left.left = new Node(4);
    root.left.right = new Node(5);
    root.right.left = new Node(6);
    root.right.right = new Node(7);
    root.left.left.left = new Node(8);
    return root;
}

/** isValuePresent()takes node of the tree and data to be checked as input and returns boolean value
 * if the data is present it returns true otherwise it returns false
 */
function isValuePresent(node, data) {
    if (node == null)
        return false;
    if (node.value == data)
        return true;
    let flag = false;
    flag = isValuePresent(node.left, data);
    if (flag)
        return flag;
    flag = isValuePresent(node.right, data);
    if (flag)
        return flag;
    return flag;
}

/** lca() function returns the lowest common ancestor of data1 &data2 
 * here the concept is check whether both value are in same subtree or not
 * if they are not in same subtree than this node is the lowest common ancestor.
 * Otherwise check this in the subtree in which both values are present
 */
function lca(root, data1, data2) {
    if (root == null)
        return null;
    if (root.value === data1 || root.value === data2)
        return root.value;
    if ((isValuePresent(root.left, data1) && isValuePresent(root.right, data2)) || (isValuePresent(root.right, data1) && isValuePresent(root.left, data2)))
        return root.value;
    if (isValuePresent(root.left, data1) && isValuePresent(root.left, data2))
        return lca(root.left, data1, data2);
    if (isValuePresent(root.right, data1) && isValuePresent(root.right, data2))
        return lca(root.right, data1, data2);

}

/**run() initialize the binary tree and call lca() function */
function run() {
    let root = getBinaryTree();
    // console.log(root.value)
    let data1 = 8;
    let data2 = 5;
    let lowestCommonAncestor = lca(root, data1, data2);
    console.log(lowestCommonAncestor);
}
run();