/** Node class represent the datatype for  the tree node which contains value field for data &
 * left feild for left node &right field for right node
 */
class Node {
    constructor(data) {
        this.left = null;
        this.value = data;
        this.right = null;
    }
}

/**getBinaryTree() returns the binary  tree */
function getBinaryTree() {
    root = new Node(1);
    root.left = new Node(2);
    root.right = new Node(3);
    root.left.left = new Node(4);
    root.left.right = new Node(5);
    root.right.right = new Node(6);
    root.right.right.left = new Node(7);
    root.right.right.right = new Node(8);

    return root;

}
/**levelTraverse() takes the list of nodes(which are at same level)
 * as input and return the right sibiling of the given value.
 */
function levelTraverse(nodeList, data) {
    //console.log("-------------------------------------");
    if (nodeList.length === 0)
        return null;
    let i = 0;
    for (i = 0; i < nodeList.length; i++) {
        // console.log(nodeList[i].value);
        if (nodeList[i].value === data) {
            //  console.log(nodeList[i].value);
            return (nodeList[i + 1] != null ? nodeList[i + 1].value : null);
        }
    }
    let elements = [];
    let levelNodes = [];
    while (nodeList.length != 0) {
        let node = nodeList.shift();
        elements.push(node.value);
        if (node.left != null)
            levelNodes.push(node.left);
        if (node.right != null)
            levelNodes.push(node.right);

    }
    return levelTraverse(levelNodes, data);

}

/** levelOrderTraversal() takes root of the tree and traverse the tree in levelorder */
function levelOrderTraversal(root, data) {
    if (root == null) {
        console.log("tree is empty");
        return null;
    }
    let items = [];
    items.push(root);
    if (root.value === data)
        return null;
    items.shift().value;
    if (root.left != null)
        items.push(root.left);
    if (root.right != null)
        items.push(root.right);
    return levelTraverse(items, data);
}

/**getRightSibiling() takes root of the tree & data of which rightshibling it should return */
function getRightSibiling(root, data) {
    return levelOrderTraversal(root, data);
}

/**run() initialize the binary tree and call getRightSibiling() function */
function run() {
    let root = getBinaryTree();
    // console.log(root.value)
    let data = 8;
    let rightSibiling = getRightSibiling(root, data);
    console.log(rightSibiling);
}
run();