/*
Name of the project : Dailychallenge
File name : display.js
Description : Traverse the linked list such that you retain M nodes then delete
              next N nodes, continue the same until end of the linked list.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : [1,2,3,4,5,6,7,8],2,2
Output : [1,2,5,6]
*/


class Node {
    constructor(element) {
        this.element = element;
        this.next = null;
    }
}


class linkedList {
    constructor() {
        this.head = null;
        this.size = 0;
    }
    //function to add elements to the linked list
    addElement(num) {
        if(this.head == null) {
            var node = new Node(num);
            this.head = node;
            this.size ++;
        }else {
            var prev ;
            var current = this.head;
            while(current) {
                prev = current;
                current = current.next;
            }
            var node = new Node(num);
            prev.next = node;
            this.size ++;
        }
    }
    //function to display the linked list
    show() {
      var curr = this.head;
      var str = "";
      while (curr) {
         str += curr.element + " ";
         curr = curr.next;
      }
      console.log(str);
    }
    makeChange(key1,key2) {
        var curr = this.head;
        var temp=this.head,prv;
        while(curr) {
            for (var i = 0; i < key1; i++) {
                if(curr) {
                      temp =curr;
                      curr = curr.next;
                }
            }
            for (var i = 0; i < key2; i++) {
                if(curr)
                    curr = curr.next;
            }
            if(curr) {
                if(curr.next)
                    temp.next = curr;
            }else {
                temp.next = null;
            }
        }
    }
}


function display(arr,key1,key2) {
    var ll = new linkedList();
    for (var i = 0; i < arr.length; i++) {
      ll.addElement(arr[i]);
    }
    ll.show();
    ll.makeChange(key1,key2);
    ll.show();
}


display([1,2,3,4,5,6,7,8],2,2);
