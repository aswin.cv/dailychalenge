/*
Name of the project : Dailychallenge
File name : display.js
Description :  Given a linked list and a number k. Reverse every k nodes in
              the list.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : [1,2,3,4,5,6,7,8],2
Output : [2,1,4,3,6,5,8,7]
*/


class Node {
    constructor(element) {
        this.element = element;
        this.next = null;
    }
}


class linkedList {
    constructor() {
        this.head = null;
        this.size = 0;
    }
    //function to add elements to the linked list
    addElement(num) {
        if(this.head == null) {
            var node = new Node(num);
            this.head = node;
            this.size ++;
        }else {
            var prev ;
            var current = this.head;
            while(current) {
                prev = current;
                current = current.next;
            }
            var node = new Node(num);
            prev.next = node;
            this.size ++;
        }
    }
    //function to display the linked list
    show() {
      var curr = this.head;
      var str = "";
      while (curr) {
         str += curr.element + " ";
         curr = curr.next;
      }
      console.log(str);
    }
    reverseList(key) {
        var root = this.head;
        this.rvrs(root,key);
    }
    rvrs(root,key) {
        if(root.next) {
            this.swap(root,root.next);
            if(root.next)
                root = root.next;
            else
                return;
            if(root.next)
                root = root.next;
            else
                return;
            this.rvrs(root);
        }else {
           return;
        }
    }
    swap(prv,root) {
      var temp;
      temp = prv.element;
      prv.element =root.element;
      root.element = temp;
    }
}


function display(arr,key) {
    var ll = new linkedList();
    for (var i = 0; i < arr.length; i++) {
      ll.addElement(arr[i]);
    }
    ll.show();
    ll.reverseList(key);
    ll.show();
}


display([1,2,3,4,5,6,7,8],2);
