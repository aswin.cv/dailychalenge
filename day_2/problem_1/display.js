/*
Name of the project : Dailychallenge
File name : display.js
Description : Remove any “unordered” elments from a linked list
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : [1,4,2,3,6,6,10,4,'x']
Output : [1,4,6,6,10,'x']
*/


class Node {
    constructor(element) {
        this.element = element;
        this.next = null;
    }
}


class linkedList {
    constructor() {
        this.head = null;
        this.size = 0;
    }
    //function to add elements to the linked list
    addElement(num) {
        if(this.head == null) {
            var node = new Node(num);
            this.head = node;
            this.size ++;
        }else {
            var prev ;
            var current = this.head;
            while(current) {
                prev = current;
                current = current.next;
            }
            var node = new Node(num);
            prev.next = node;
            this.size ++;
        }
    }
    //function to display the linked list
    show() {
      var curr = this.head;
      var str = "";
      while (curr) {
         str += curr.element + " ";
         curr = curr.next;
      }
      console.log(str);
    }
    makeChange() {
        var curr = this.head;
        var temp,prv=this;
        while(curr) {
            if(curr!=this.head) {
                if(parseInt(prv.element) > parseInt(curr.element)) {
                    prv.next = curr.next;
                    curr = curr.next;
                }else {
                  prv = curr;
                  curr = curr.next;
                }
            }else {
               prv = curr;
               curr =curr.next;
            }
        }
    }
}


function display(arr) {
    var ll = new linkedList();
    for (var i = 0; i < arr.length; i++) {
      ll.addElement(arr[i]);
    }
    ll.show();
    ll.makeChange();
    ll.show();
}


display([1,4,2,3,6,6,10,4,'x']);
